//>> pure-amd
define([
    'dojo', 
    'dojo/json',
    'dojo/date',
    './util',
    'dojo/_base/lang'
], function (
    dojo,
    json,
    date,
    util
    ) {


    return dojo.declare('kinsey/BrowserCache', [], {

        constructor: function(id) {
            this.id = id;
            if ( this.test() ) {
                this.cache = window.localStorage;
            }
        },

        test: function() {
            var uid = new Date(),
                storage,
                result;
            try {
                (storage = window.localStorage).setItem(uid, uid);
                result = storage.getItem(uid) == uid;
                storage.removeItem(uid);
                return result && storage;
            } catch(e) {}
        },

        stash: function(data, expireDays) {
            var daysToExpire = expireDays || 1;
            if (this.cache) {
                var expiry = date.add(new Date(), 'day', daysToExpire);
                this.cache.setItem(this.id, json.stringify({
                    expires: expiry.toString(),
                    data: data  
                }));
            }
        },

        fetch: function(defaultValue) {
            if (this.cache) {
                var today = new Date();
                cache = json.parse(this.cache.getItem(this.id), true);
                if ( cache ) {
                    console.debug("expiry date from cache: " + cache.expires);
                    console.debug(util);
                    var expiry = new Date(cache.expires);
                    console.debug("translated expiries date: " + expiry);
                    if ( !isNaN(expiry) && expiry > today) {
                        console.debug("returning cache");
                        this.rawObjectsFromCache = true;
                        return cache.data;
                    } else {
                        console.debug("cache has expired");
                        this.clear();
                    }
                }
            }
            console.debug("no acceptable cache, return default");
            if ( defaultValue ) {
                return dojo.clone(defaultValue);
            }
        },

        exists: function() {
            if (this.cache) {
                return this.id in this.cache;
            }
        },

        clear: function() {
            if (this.cache) {
                this.cache.clear(this.id);
            }
        }


    });

});
