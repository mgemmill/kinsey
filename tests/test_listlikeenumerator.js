//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main', 
    'kinsey/collections/ListLikeEnumerator'
], function (
    dojo,
    string,
    doh, 
    ListLikeEnumerator  
    ) {
    
    console.debug("defining test_listlikeenumerator");

    function assertList(list, expected, message) {
        var output = '';
        list.iter(function(item) {
            console.debug(item);
            output += item.data;
        });
        doh.is(expected, output, message);
    }

    doh.register( "app.tools.ListLikeEnumerator", [
        
        { 
            name: "ListLikeEnumerator raw initialization",
            runTest: function() {
                var list = new ListLikeEnumerator();
                doh.is(100, list.indexes.length, "indexes length should be 100.");
            }
        },

        {
            name: "ListLikeEnumerator init with object",
            runTest: function() {
                var list = new ListLikeEnumerator(),
                    object = { "0": "zero", "1": "one" },
                    list2 = new ListLikeEnumerator(object);
                list.initialize(object);
                doh.is(98, list.indexes.length, "indexes length should be 98.");
                doh.is(98, list2.indexes.length, "indexes length should be 98.");
            }
        },
        {
            name: "ListLikeEnumerator next",
            runTest: function() {
                var list = new ListLikeEnumerator(),
                    object = { "0": "zero", "1": "one" };
                list.initialize(object);
                doh.is(98, list.indexes.length, "indexes length should be 98.");
                doh.is(2, list.next(), "next value should be '2'");
                doh.is(97, list.indexes.length, "indexes length should be 97.");
                doh.is(3, list.next(), "next value should be '3'");
                doh.is(96, list.indexes.length, "indexes length should be 96.");
            }
        }

    ]);

});
