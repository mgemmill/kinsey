//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main', 
    'dojo/robot',
    'app/binding',
    'app/validation',
    'app/testing',
    'dojo/_base/declare'
], function (
    dojo,
    string,
    doh, 
    robot,
    binding,
    validation,
    testing
    ) {
    
    var sub = string.substitute,
        Binding = binding.Binding;

    console.debug("defining test_binding");

    function createOne() {
        var binding = new Binding({ 'text-input': 'textBox',
                                    'textarea-input': 'textArea',
                                    'select-input': 'select',
                                    'checkbox-input': 'checkbox',
                                    'radio-input': 'radio'}),
            obj = { 'textBox': 'textbox',
                    'textArea': 'textarea',
                    'select': 'one',
                    'checkbox': true,
                    'radio': 'three'};

            return [binding, obj];

    }

    function createValidator() {
        var validator = new Validator(
                dojo.byId("text-input"),
                function (node, object) {
                    throw new Error("THIS WILL ALWAYS FAIL!");
                });
        return validator;
    }
    

    function unitTest(test) {
        return dojo.safeMixin({
            setUp: function() {
                var test = createOne();
                this.binding = test[0]; 
                this.object = test[1];
            }
        }, test);
    }

    doh.register( "ErrorObject Tests", [
        {
            name: "ErrorObject test errors.",
            runTest: function() {
                var errors = new validation.ErrorObject();
                errors.add(new Error("a"));
                doh.is(1, errors.errors.length);
                doh.is(0, errors.warnings.length);
                doh.f(errors.fullyValid(), "ErrorObject is not fully valid.");
                doh.t(errors.hasErrors(), "ErrorObject has errors!");
                doh.f(errors.warningsOnly(), "ErrorObject has errors!.");
            }
        },
        {
            name: "ErrorObject test warnings.",
            runTest: function() {
                var errors = new validation.ErrorObject();
                errors.add(new validation.Warning("a"));
                doh.is(0, errors.errors.length);
                doh.is(1, errors.warnings.length);
                doh.f(errors.fullyValid(), "ErrorObject is not fully valid.");
                doh.t(errors.hasErrors(), "ErrorObject has errors!");
                doh.t(errors.warningsOnly(), "ErrorObject has warnings only!.");
            }
        }
    ]);

    doh.register( "ErrorCollection Tests", [
        {
            name: "ErrorCollection test errors.",
            runTest: function() {
                var err1 = new validation.ErrorObject(),
                    errors = new validation.ErrorCollection();
                err1.add(new Error("a"));
                errors.add(err1);

                doh.is(1, errors.errorObjects.length);
                doh.t(errors.hasErrors(), "ErrorObject has errors!");
                doh.f(errors.warningsOnly(), "ErrorObject has errors not just warnings!.");
            }
        },
        {
            name: "ErrorCollection test warnings.",
            runTest: function() {
                var err1 = new validation.ErrorObject(),
                    errors = new validation.ErrorCollection();
                err1.add(new validation.Warning("a"));
                errors.add(err1);

                doh.is(1, errors.errorObjects.length);
                doh.t(errors.hasErrors(), "ErrorObject has errors!");
                doh.t(errors.warningsOnly(), "ErrorObject has errors not just warnings!.");
            }
        },
        {
            name: "ErrorCollection with no errors.",
            runTest: function() {
                var err1 = new validation.ErrorObject(),
                    errors = new validation.ErrorCollection();
                errors.add(err1);

                doh.is(1, errors.errorObjects.length);
                doh.f(errors.hasErrors(), "ErrorObject has no errors!");
                doh.t(errors.warningsOnly(), "ErrorObject has just warnings or better!.");
            }
        },
        {
            name: "ErrorCollection with no multiple errors.",
            runTest: function() {
                var err1 = new validation.ErrorObject(),
                    err2 = new validation.ErrorObject(),
                    errors = new validation.ErrorCollection();
                errors.add(err1);
                errors.add(err2);

                doh.is(2, errors.errorObjects.length);
                doh.f(errors.hasErrors(), "ErrorObject has no errors!");
                doh.t(errors.warningsOnly(), "ErrorObject has just warnings or better!.");

                err1.add(new Error("a"));
                doh.t(errors.hasErrors(), "ErrorObject has errors!");
                doh.f(errors.warningsOnly(), "ErrorObject has errors and no warnings!.");
            }
        }
    ]);
    doh.register( "Validation Object Tests", [
        {
            name: "Validator initialization",
            runTest: function() {

                var validator = new validation.Validator();
                // failing
                doh.is(undefined, validator.node);
                doh.is(0, validator.rules.length);

                validator = createValidator();
                // passing
                doh.is('INPUT', validator.node.tagName);
                doh.is(1, validator.rules.length);
            }
        },
        {
            name: "Validator.validate",
            runTest: function() {
                var validator = createValidator(),
                    theNode,
                    theErrors;    

                validator.validate({}, function(node, errors) {
                    theNode = node;
                    theErrors = errors;
                });

                doh.is('INPUT', theNode.tagName);
                doh.is('THIS WILL ALWAYS FAIL!', theErrors.errors[0].message);
            }
        },
        {
            name: "Validator.validate ???",
            runTest: function() {
                
            }
        }

    ]);

    doh.register( "Binding.validate Tests", [
        unitTest({
            name: "Binding.setValidators",
            runTest: function() {
                testing.assertErrorMessage("You must provide one or more validator functions.", this.binding.setValidators);
            }
        }),
        unitTest({
            name: "Binding.setValidators with a bogus property name.",
            runTest: function() {
                var that = this;
                testing.assertErrorMessage("Binding object does not contain a map with id: bogusPropertyName", function() {
                    that.binding.setValidators({bogusPropertyName: function(object, handler) {}});
                });
            }
        }),
        unitTest({
            name: "Binding.setValidators sets validators",
            runTest: function() {
                this.binding.setValidators({textBox: function(object, handler) {}});
                // test result here.
                doh.is(1, this.binding.validators.length, "There should be 1 validator in the binding.");
            }
        }),
        unitTest({
            name: "Binding.validate simple test",
            runTest: function() {
                var theNode,
                    theErrors,
                    errorHandler = function (node, errors) {
                        theNode = node;
                        theErrors = errors;
                    };
                this.binding.setValidators({textBox: function (object, node) { throw Error("FAILURE!"); }});
                this.binding.bind(this.object);
                this.binding.validate(errorHandler);
                doh.is(testing.quote("INPUT"), testing.quote(theNode.tagName));
                doh.is(testing.quote("FAILURE!"), testing.quote(theErrors.errors[0].message));
            }

        }),
        unitTest({
            name: "Binding.validate multiple validators testing...",
            runTest: function() {
                var nodes = [],
                    errors,
                    errorHandler = function (node, error) {
                        nodes.push(node);
                    },
                    validator = function (object, node) {
                        throw Error(subs("ERROR ON ${0}", [node.tagName]));
                    };
                this.binding.setValidators({textBox: [validator, validator], textArea: validator });
                this.binding.bind(this.object);
                errors = this.binding.validate(errorHandler);
                console.debug(nodes);
                console.debug(errors);
                doh.is(2, nodes.length, "There should be 2 nodes.");
                doh.is(2, errors.errorObjects.length, "There should be 2 ErrorObjects.");
                doh.is(2, errors.errorObjects[0].errors.length, "The first ErrorObject should have 2 errors.");
            }
        })
    ]);
        
});

