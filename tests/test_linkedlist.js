//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main', 
    'kinsey/collections/LinkedList'
], function (
    dojo,
    string,
    doh, 
    LinkedList 
    ) {
    
    var sub = string.substitute;

    console.debug("defining test_linkedlist");

    function assertList(list, expected, message) {
        var output = '';
        list.iter(function(item) {
            console.debug(item);
            output += item.data;
        });
        doh.is(expected, output, message);
    }

    doh.register( "app.tools.LinkedList", [
        
        { 
            name: "LinkedList Initialization",
            runTest: function() {
                var list = new LinkedList();
                doh.is(0, list.length, "List length should be 0.");
            }
        },

        {
            name: "LinkedList add data",
            runTest: function() {
                var list = new LinkedList();
                list.append(1);
                doh.is(1, list.length, "List length should be 1");
                list.append(2);
                doh.is(2, list.length, "List length should be 2");
            }
        },

        {
            name: "LinkedList iter data",
            runTest: function() {
                var list = new LinkedList();
                for ( var i=0; i < 20; i++) {
                    list.append(i);
                }
                var output = '';
                list.iter(function(item) {
                    console.debug(item);
                    output += item.data;
                });
                doh.is(20, list.length, "List length should be 20.");
                doh.is('012345678910111213141516171819', output, "Iter should have returned `0123456789011213141516171819`.");
                
            }
        },

        {
            name: "LinkedList find data.",
            runTest: function() {
                var list = new LinkedList(),
                    found;
                list.append(2);
                list.append(3);
                list.append(4);
                list.append(5);
                list.append(6);
                found = list.find(4);
                doh.is(4, found.data, "Should have found data `4`.");
            }
        },

        {
            name: "LinkedList insert data.",
            runTest: function() {
                var list = new LinkedList(),
                    found,
                    inserted;
                list.append(2);
                list.append(3);
                list.append(4);
                list.append(5);
                list.append(6);
                inserted = list.insert(4, 9);
                doh.is(9, inserted.data, "Should have inserted data `9`.");
                doh.is(4, inserted.previous.data, "Inserted previous data should be `4`.");
                doh.is(5, inserted.next.data, "Inserted next data should be `5`.");
                assertList(list, "234956", "List should now be `234956`.");
            }
        },

        {
            name: "LinkedList get index",
            runTest: function() {
                var list = new LinkedList(),
                    found;
                list.append(0);
                list.append(1);
                list.append(2);
                list.append(3);
                list.append(4);

                found = list.getIndex(2);
                doh.is(2, found.data, "Index 2 should return value 2.");
                found = list.getIndex(4);
                doh.is(4, found.data, "Index 4 should return value 4.");
                    
            }
        },
       {
           name: "LinkedList.remove",
           runTest: function() {
                var list = new LinkedList(),
                    found;
                list.append(0);
                list.append(1);
                list.append(2);
                list.append(3);
                list.append(4);

                assertList(list, "01234");
                found = list.remove(2);
                doh.is(2, found.data, "removed item should return value 2.");
                assertList(list, "0134");
               
           }
       },
       {
            name: "LinkedList.Node.isFirst....",
            runTest: function() {
                var list = new LinkedList();
                function assertFirstLast() {
                    doh.t(list.first.isFirst(), "First nodes' isFirst should be true.");
                    doh.t(list.last.isLast(), "Last nodes' isLast should be true.");
                }
                list.append(1);
                assertFirstLast();
                list.append(2);
                assertFirstLast();
                list.append(3);
                assertFirstLast();
                var found = list.getIndex(1);
                doh.is(false, found.isFirst());
                doh.is(false, found.isLast());
            }
        }

    ]);

});

