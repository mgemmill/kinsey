//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main', 
    'kinsey/collections/BrowserStack'
], function (
    dojo,
    string,
    doh, 
    BrowserStack 
    ) {
    
    var sub = string.substitute;

    console.debug("defining test_circularlist");

    function assertList(list, expected, message) {
        var output = '';
        list.iter(function(item) {
            console.debug(item);
            output += item.data;
        });
        doh.is(expected, output, message);
    }

    doh.register( "kinsey.collections.BrowserStack", [
        
        { 
            name: "BrowserStack Empty Initialization",
            runTest: function() {
                var stack = new BrowserStack();
                doh.is(0, stack.length, "BrowserStack length should be 0.");
            }
        },
        {
            name: "BrowserStack initialize with array",
            runTest: function() {
                var stack = new BrowserStack([0,1,2,3,4,5,6,7,8,9]);
                doh.is(10, stack.length, "BrowserStack length should be 10.");
                doh.is(9, stack.current.data, "Current obj should be 9");
            }
        },
        {
            name: "BrowserStack backward",
            runTest: function() {
                var stack = new BrowserStack([0,1,2,3,4,5,6,7,8,9]),
                    current;
                current = stack.backward();
                doh.is(8, current, "Current should be 8");
                doh.is(8, stack.current.data, "Current should be 8");

                stack.backward(); // 7
                stack.backward(); // 6
                stack.backward(); // 5
                stack.backward(); // 4
                stack.backward(); // 3
                stack.backward(); // 2
                stack.backward(); // 1

                current= stack.backward(); // 0
                doh.is(0, current, "Current should be 0");
                doh.is(0, stack.current.data, "Current should be 0");

                current = stack.backward(); // 0
                doh.is(0, current, "Current should be 0");
                doh.is(0, stack.current.data, "Current should be 0");

            }
        },
        {
            name: "BrowserStack forward",
            runTest: function() {
                var stack = new BrowserStack([0,1,2,3,4,5,6,7,8,9]),
                    current;
                current = stack.forward();
                doh.is(9, current, "Current should be 9");
                doh.is(9, stack.current.data, "Current should be 9");
                stack.backward(); // 8
                stack.backward(); // 7
                stack.backward(); // 6
                stack.backward(); // 5
                current = stack.forward();
                doh.is(6, current, "Current should be 6");
                doh.is(6, stack.current.data, "Current should be 6");
            }
        },
        {
            name: "BrowserStack.countToStart & countToEnd",
            runTest: function () {
                var stack = new BrowserStack();
                doh.is(0, stack.length, "stack should ave a length of 0");
                doh.is(0, stack.countToStart(), "countToStart should return 0");
                doh.is(0, stack.countToEnd(), "countToEnd should return 0");
                stack.push('one');
                doh.is(1, stack.length, "stack should have a length of 1");
                doh.is(0, stack.countToStart(), "countToStart should still return 0");
                doh.is(0, stack.countToEnd(), "countToEnd should return 0");
                stack.push('two');
                doh.is(2, stack.length, "stack should now have a length of 2");
                doh.is(1, stack.countToStart(), "countToStart should now return 1");
                doh.is(0, stack.countToEnd(), "countToEnd should return 0");
                stack.push('three');
                doh.is(3, stack.length, "stack should now have a length of 3");
                doh.is(2, stack.countToStart(), "countToStart should now return 2");
                doh.is(0, stack.countToEnd(), "countToEnd should return 0");
                stack.push('four');
                doh.is(4, stack.length, "stack should now have a length of 4");
                doh.is(3, stack.countToStart(), "countToStart should now return 3");
                doh.is(0, stack.countToEnd(), "countToEnd should return 0");
                stack.backward();
                doh.is(4, stack.length, "stack should now have a length of 4");
                doh.is(2, stack.countToStart(), "countToStart should now return 2");
                doh.is(1, stack.countToEnd(), "countToEnd should return 1");
                stack.backward();
                doh.is(4, stack.length, "stack should now have a length of 4");
                doh.is(1, stack.countToStart(), "countToStart should now return 1");
                doh.is(2, stack.countToEnd(), "countToEnd should return 2");
                stack.push("five");
                doh.is(3, stack.length, "stack should now have a length of 3");
                doh.is(2, stack.countToStart(), "countToStart should now return 2");
                doh.is(0, stack.countToEnd(), "countToEnd should return 0");

            }
        },
        {
            name: "BrowserStack.toStart",
            runTest: function() {
                var stack = new BrowserStack([0,1,2,3,4,5,6,7,8,9]);
                stack.toStart();
                doh.is(0, stack.current.data, "Currend should be 0");
                stack.forward();
                stack.forward();
                stack.forward();
                stack.forward();
                doh.is(4, stack.current.data, "Currend should be 0");
                stack.toStart();
                doh.is(0, stack.current.data, "Currend should be 0");
                
            }
        },

        {
            name: "BrowserStack.toEnd",
            runTest: function() {
                var stack = new BrowserStack([0,1,2,3,4,5,6,7,8,9]);
                stack.toStart();
                doh.is(0, stack.current.data, "Currend should be 0");
                stack.toEnd();
                doh.is(9, stack.current.data, "Currend should be 9");
                stack.backward();
                stack.backward();
                stack.backward();
                doh.is(6, stack.current.data, "Currend should be 6");
                stack.toEnd();
                doh.is(9, stack.current.data, "Currend should be 9");
            }
        },
        {
            name: "BrowserStack.toArray",
            runTest: function() {
                var stack = new BrowserStack([0,1,2,3,4,5,6,7,8,9]);
                doh.is([0,1,2,3,4,5,6,7,8,9], stack.toArray(), "");
            }
        },
        {
            name: "BrowserStack chop",
            runTest: function() {
                var stack = new BrowserStack([0,1,2,3,4,5,6,7,8,9]),
                    current;
                current = stack.backward(); // 8
                current = stack.backward(); // 7
                current = stack.backward(); // 6
                doh.is(6, current, "Current should be 6");
                doh.is(6, stack.current.data, "Current should be 6");
                stack.push(22);
                doh.is(8, stack.length, "Stack length should be 8.");
                doh.is([0,1,2,3,4,5,6,22], stack.toArray(), '');
            }
       }

    ]);

});

