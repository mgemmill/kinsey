//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main', 
    'kinsey/collections/CircularList'
], function (
    dojo,
    string,
    doh, 
    CircularList 
    ) {
    
    var sub = string.substitute;

    console.debug("defining test_circularlist");

    function assertList(list, expected, message) {
        var output = '';
        list.iter(function(item) {
            console.debug(item);
            output += item.data;
        });
        doh.is(expected, output, message);
    }

    doh.register( "app.tools.CircularList", [
        
        { 
            name: "CircularList Initialization",
            runTest: function() {
                var list = new CircularList();
                doh.is(0, list.length, "List length should be 0.");
            }
        },
        {
            name: "CircularList.append first",
            setUp: function() {
            },
            runTest: function() {
                var list = new CircularList();
                list.append(1);
                doh.is(1, list.length, "List length should be 1.");
                doh.is(1, list.current.data, "Current item shold be 1.");
                doh.is(1, list.current.next.data, "Next item shold be 1.");
                doh.is(1, list.current.previous.data, "Previous item shold be 1.");
            },
            tearDown: function() {
            }
        },
        {
            name: "CircularList.append multiple",
            setUp: function() {
            },
            runTest: function() {
                var list = new CircularList();
                list.append(1);
                list.append(2);
                doh.is(2, list.length, "List length should be 2.");
                doh.is(2, list.current.data, "Current item shold be 2.");
                doh.is(1, list.current.next.data, "Next item shold be 1.");
                doh.is(1, list.current.previous.data, "Previous item shold be 1.");
            },
            tearDown: function() {
            }
        },
        {
            name: "CircularList.next",
            assertCurrent: function(list, current) {
                doh.is(current, list.current.data, "Current item should be: " + current);
            },
            runTest: function() {
                var list = new CircularList();
                list.append(1);
                list.append(2);
                list.append(3);
                list.append(4);

                doh.is(4, list.length, "List length should be 2.");
                this.assertCurrent(list, 4);
                list.next();
                this.assertCurrent(list, 1);
                list.next();
                this.assertCurrent(list, 2);
                list.next();
                this.assertCurrent(list, 3);
                list.next();
                this.assertCurrent(list, 4);
                list.next();
                this.assertCurrent(list, 1);
            }
        }

    ]);

});

