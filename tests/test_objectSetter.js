//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main', 
    'dojo/robot',
    'app/binding',
    'app/testing'
], function (
    dojo,
    string,
    doh, 
    robot,
    binding,
    testing 
    ) {
    
    var sub = string.substitute,
        quote = testing.quote,
        Property = binding.Property;

    console.debug("defining test_objectSetter");

    function unitTest(test) {
        return dojo.safeMixin({
            setUp: function() {
                this.object = {
                    name: "Yeller",
                    pups: [
                        { name: "sima" },
                        { name: "shina" }
                    ]
                };
            }
        }, test);
    }
    doh.register( "app.binding.Property", [
        unitTest({
            name: "Property.set simple property",
            runTest: function() {
                var prop = new Property(this.object, 'name');
                prop.set("Finlay");
                doh.is(quote("Finlay"), quote(this.object.name));
            }
        }),
        unitTest({
            name: "Property.set nested property #1", 
            runTest: function() {
                var prop = Property(this.object, 'pups');
                prop.set(['tada!']);
                doh.is(quote('tada!'), quote(this.object.pups[0]));
            }
        }),
        unitTest({
            name: "Property.set nested property #2", 
            runTest: function() {
                var prop = Property(this.object, 'pups.0');
                prop.set({name: "Woof"});
                doh.is(quote('Woof'), quote(this.object.pups[0].name));
            }
        }),
        unitTest({
            name: "Property.set nested property #3", 
            runTest: function() {
                var prop = Property(this.object, 'pups.0.name');
                prop.set("Sammy");
                doh.is(quote('Sammy'), quote(this.object.pups[0].name));
            }
        }),
        unitTest({
            name: "Property.set nested property #4", 
            runTest: function() {
                var prop = Property(this.object, 'pups.0.age');
                prop.set(23);
                doh.is(quote(23), quote(this.object.pups[0].age));
            }
        }),
        unitTest({
            name: "Property.get simple property",
            runTest: function () {
                var prop = Property(this.object, 'name');
                doh.is(quote("Yeller"), quote(prop.get()));
            }
        }),
        unitTest({
            name: "Property.get nested property #1",
            runTest: function () {
                var prop = Property(this.object, 'pups');
                doh.is(quote(2), quote(prop.get().length));
            }
        }),
        unitTest({
            name: "Property.get nested property #2",
            runTest: function () {
                var prop = Property(this.object, 'pups.0');
                doh.is(quote("sima"), quote(prop.get().name));
            }
        }),
        unitTest({
            name: "Property.get nested property #3",
            runTest: function () {
                var prop = Property(this.object, 'pups.1.name');
                doh.is(quote("shina"), quote(prop.get()));
            }
        }),
        unitTest({
            name: "Property.get nested property #4",
            runTest: function () {
                var prop = Property(this.object, 'pups.1.age');
                doh.is(quote("undefined"), quote(prop.get()));
            }
        }),
        unitTest({
            name: "Property.get nested property #4",
            runTest: function () {
                var prop = Property(this.object, 'pups.1.age.string');
                testing.assertErrorMessage(
                    "Cannot access Property segment `string` of `age` because it's parent is undefined.", function() { prop.get(); });
            }
        })
    ]);

        
});

