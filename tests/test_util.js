//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main', 
    'kinsey/util',
    'kinsey/testing'
], function (
    dojo,
    string,
    doh, 
    util,
    testing
    ) {
    
    var sub = string.substitute,
        quote = testing.quote;

    console.debug("defining test_util");

    function assertUrl(url, expected) {   //// }protocol, host, port, path, query, params, file, hash, relative, segments) {
        doh.is(quote(url.protocol), quote(expected.protocol), "testing protocol");
        doh.is(quote(url.host), quote(expected.host), "testing host");
        doh.is(quote(url.port), quote(expected.port), "testing port");
        doh.is(quote(url.path), quote(expected.path), "testing path");
        doh.is(quote(url.query), quote(expected.query), "testing query");
        doh.is(url.params, expected.params, "testing params");
        doh.is(quote(url.file), quote(expected.file), "testing file");
        doh.is(quote(url.hash), quote(expected.hash), "testing hash");
        doh.is(quote(url.segments), quote(expected.segments || ""), "testing segments");
    }

    doh.register( "util.isEmpty Tests", [
        {
            name: "util.isEmpty testing...",
            runTest: function() {
               // are empty
               doh.is(quote(util.isEmpty({})), quote(true), "{} should be empty!");
               doh.is(quote(util.isEmpty([])), quote(true), "[] should be empty!");
               doh.is(quote(util.isEmpty("")), quote(true), "'' should be empty!");
               // are not empty
               doh.is(quote(util.isEmpty({ name: true })), quote(false), "{} is not empty!");
               doh.is(quote(util.isEmpty([ 1, 2, 3])), quote(false), "[] is not empty!");
               doh.is(quote(util.isEmpty("non-empty string")), quote(false), "'' is not empty!");
            }
        }    
    ]);

    doh.register( "util.isDate Tests", [
        {
            name: "util.isDate testing legitimate values...",
            runTest: function() {
                doh.is(util.isDate(new Date()), true, "should return true");    
                doh.is(util.isDate("11-01-2012"), true, "should return true");
                doh.is(util.isDate(0), true, "should return true");
            }
        },
        {
            name: "util.isDate testing bad values...",
            runTest: function() {
                doh.is(util.isDate(""), false, "should return false");    
                doh.is(util.isDate({}), false, "should return false");    
                doh.is(util.isDate([]), false, "should return false");    
            }
        }

    ]);
    

    doh.register( "util.generateUUID Tests", [
        {
            name: "util.generateUUID testing...",
            runTest: function() {
               // are empty
               regex = /^[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[a-f0-9]{4}-[a-f0-9]{12}$/;
               for (var i = 0; i < 100; i++) {
                   var uuid = util.generateUUID();
                   doh.is(36, uuid.length);
                   doh.is(true, regex.test(uuid));
               }
            }
        }    
    ]);

    doh.register( "util.UrlBuilder Tests", [
        {
            name: "UrlBuilder test basic url",
            runTest: function() {
                var url = new util.UrlBuilder();   
                doh.is(quote("http://127.0.0.1"), quote(url.build()), "Url should build to: http://127.0.0.1");
                doh.is(quote("/"), quote(url.buildRelative()), "Url should build to: http://127.0.0.1");
            }
        },        
        {
            name: "UrlBuilder test with port",
            runTest: function() {
                var url = new util.UrlBuilder({ port: 8080 });
                doh.is(quote("http://127.0.0.1:8080"), quote(url.build()), "Url should build to: http://127.0.0.1");
                doh.is(quote("/"), quote(url.buildRelative()), "Url should build to: http://127.0.0.1");
            }
        },        
        {
            name: "UrlBuilder test with hash",
            runTest: function() {
                var url = new util.UrlBuilder({  
                        port: 8080,
                        hash: "hash-tag"
                    });
                doh.is(quote("http://127.0.0.1:8080#hash-tag"), quote(url.build()), "Url should build to: http://127.0.0.1");
                doh.is(quote("/#hash-tag"), quote(url.buildRelative()), "Url should build to: http://127.0.0.1");
            }
        },        
        {
            name: "UrlBuilder test with query",
            runTest: function() {
                var url = new util.UrlBuilder({  
                        port: 8080,
                        hash: "hash-tag",
                        query: { tag: "doc", id: 23 }
                    });
                doh.is(quote("http://127.0.0.1:8080?tag=doc&id=23#hash-tag"), quote(url.build()), "Url should build to: http://127.0.0.1");
                doh.is(quote("/?tag=doc&id=23#hash-tag"), quote(url.buildRelative()), "Url should build to: http://127.0.0.1");
            }
        },        
        {
            name: "UrlBuilder test with options",
            runTest: function() {
                var url = new util.UrlBuilder({
                    protocol: "https",
                    host: "localhost",
                    port: 8088,
                    path: "path/location",
                    query: "tag=doc&id=23",
                    hash: "hash-thing"
                });   
                console.debug(url);
                doh.is(quote("https://localhost:8088/path/location?tag=doc&id=23#hash-thing"), quote(url.build()), "Url should build to: http://127.0.0.1");
                doh.is(quote("/path/location?tag=doc&id=23#hash-thing"), quote(url.buildRelative()), "Url should build to: http://127.0.0.1");
            }
        },
        {
            name: "UrlBuilder test build with options",
            runTest: function() {
                var url = new util.UrlBuilder({
                                protocol: "https",
                                host: "localhost",
                                port: 8088,
                                path: "path/location",
                                query: "tag=doc&id=23",
                                hash: "hash-thing"
                                }),   
                    query = { query: { fetch: "things", width: 23 }, hash: "", port: 80};

                doh.is(quote("https://localhost/path/location?fetch=things&width=23"), quote(url.build(query)));
                doh.is(quote("https://localhost:8088/path/location?tag=doc&id=23#hash-thing"), quote(url.build()));

            }
        },
        {
            name: "UrlBulder from UrlParser test",
            runTest: function() {
                var url = "http://localhost:8080/path/somewhere",
                    parser = new util.UrlParser(url),
                    builder = new util.UrlBuilder(parser);
                doh.is(quote(url), quote(builder.build()));
                doh.is(quote("/path/somewhere"), quote(builder.buildRelative()));
            }
        },
        {
            name: "UrlBulder from UrlParser with standard....",
            runTest: function() {
                var url = "http://localhost:8080/dojo/kinsey/tests/test_util.html",
                    parser = new util.UrlParser(url),
                    builder = new util.UrlBuilder(parser);
                doh.is(quote(url), quote(builder.build()));
                builder.setUrl('path', 'store.cgi');
                doh.is(quote('http://localhost:8080/store.cgi'), quote(builder.build()));
            }
        }
    ]);

    doh.register( "util.UrlParser Tests", [
        {
            name: "UrlParser test basic url.",
            runTest: function() {
                var url = new util.UrlParser("http://localhost:8080");
                doh.is(url.source, "http://localhost:8080");
                assertUrl(url, {
                            protocol: "http", 
                            host: "localhost", 
                            port: "8080", 
                            path: "/",
                            params: {},
                            relative: "/",
                            segments: "",
                            query: "",
                            file: "",
                            hash: ""
                });
            }
        },
        {
            name: "UrlParser test url with path.",
            runTest: function() {
                var url = new util.UrlParser("http://localhost:8080/this/path");
                assertUrl(url, {
                            protocol: "http", 
                            host: "localhost", 
                            port: "8080", 
                            path: "/this/path",
                            params: {},
                            relative: "/this/path",
                            segments: ['this', 'path'],
                            query: "",
                            file: "path",
                            hash: ""
                });
            }
        },
        {
            name: "UrlParser test url with path and hash.",
            runTest: function() {
                var url = new util.UrlParser("http://localhost:8080/this/path.html#noob");
                assertUrl(url, {
                            protocol: "http", 
                            host: "localhost", 
                            port: "8080", 
                            path: "/this/path.html",
                            params: {},
                            relative: "/this/path.html#noob",
                            segments: ['this', 'path.html'],
                            query: "",
                            file: "path.html",
                            hash: "noob"
                });
            }
        },
        {
            name: "UrlParser test url with path and query.",
            runTest: function() {
                var url = new util.UrlParser("http://localhost:8080/this/path.html?poo=bear&dingo=django");
                assertUrl(url, {
                            protocol: "http", 
                            host: "localhost", 
                            port: "8080", 
                            path: "/this/path.html",
                            params: { "poo": "bear", "dingo": "django"},
                            relative: "/this/path.html?poo=bear&dingo=django",
                            segments: ['this', 'path.html'],
                            query: "?poo=bear&dingo=django",
                            file: "path.html",
                            hash: ""
                });
            }
        },
        {
            name: "UrlParser test url http://localhost:8090/dojo/kinsey/tests/test_util.html.",
            runTest: function() {
                var url = new util.UrlParser("http://localhost:8080/dojo/kinsey/tests/test_util.html");
                assertUrl(url, {
                            protocol: "http", 
                            host: "localhost", 
                            port: "8080", 
                            path: "/dojo/kinsey/tests/test_util.html",
                            params: {},
                            relative: "/this/path.html",
                            segments: ['dojo', 'kinsey', 'tests','test_util.html'],
                            query: "",
                            file: "test_util.html",
                            hash: ""
                });
            }
        }
    ]);
        
});

