//>> pure-amd
define([
    'dojo',
    'dojo/fx',
    'kinsey/util',
    'kinsey/collections/CircularList',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-style',
    'dojo/fx/Toggler'
], function (
    dojo,
    fx,
    util,
    CircularList
    ) {
    

    return dojo.declare('dojo/fx/ImageRollover', [], {

        defaultFadeArgs: {
            duration: 500,
            delay: 0
        },

        constructor: function(interval, firstImage, secondImage, fadeArgs) {
            this.fadeArgs = {};
            dojo.safeMixin(this.fadeArgs, fadeArgs);
            this.firstImage = firstImage;
            this.secondImage = secondImage;
            this.images = new CircularList(); 
            this.imageNodes = new CircularList();
            this.imageNodes.append(firstImage);
            this.imageNodes.append(secondImage);
            this.imageNodes.next();
            console.debug("first image to fade out: " + this.imageNodes.current.data.src);
            this.timer = new util.Timer(interval);
            this.timer.on('tick', dojo.hitch(this, this.rollover));
        },

        load: function(list) {
            var that = this;
            dojo.forEach(list, function ( item ) {
                console.debug(item.src);
                that.images.append(item);
            });
            this.images.next();
            console.debug("first image to fade in: " + this.images.current.data.src);
        },

        start: function() {
            this.timer.start();
        },
        
        rollover: function (evt) {
            console.debug("rolling over image....");

            var imgToFadeOut = this.imageNodes.current.data,
                imgToFadeIn = this.imageNodes.next().data;
                nextImageSrc   = this.images.next().data;

            console.debug("fading out " + imgToFadeOut.src);
            console.debug("fading in" + imgToFadeIn.src);
            
            imgToFadeIn.src = nextImageSrc.src;

            dojo.fx.combine([
                this.fadeOut(imgToFadeOut),
                this.fadeIn(imgToFadeIn)
                ]).play();

        },

        fadeOut: function(img) {
            var args = dojo.mixin({node: img}, this.fadeArgs);
            return dojo.fadeOut(args); 
        },

        fadeIn: function(img) {
            args = dojo.mixin({node: img}, this.fadeArgs);
            return dojo.fadeIn(args); 
        }
    });
});
