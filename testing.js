//>> pure-amd
define([
    'dojo',
    'dojo/string',
    'util/doh/main' 
], function (
    dojo,
    string,
    doh
    ) {
    
    var subs = string.substitute;
    
    function quote (value) {
        if (value === undefined) {
            value = "undefined";
        } else if (value === null) {
            value = "null";
        }
        return subs('"${0}"', [value]);
    }

    return {

        quote: quote,

        assertErrorMessage: function(expected, errorTest, message) {
            try {
                errorTest();
                doh.t(false, subs("No error was thrown! Expecting: ${0}", [expected]));
            } catch (e) {
                errMsg = message || "Test did not throw the error message you were expecting.";
                doh.is(quote(expected), quote(e.message), errMsg); 
            }
        }
    };

});
