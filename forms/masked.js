define([
    "dojo",
    "dojo/_base/declare"
], function (
    dojo
) {

    var InputMask = dojo.declare('kinsey/forms/masked/InputMast', [], {

        getKey: function (evt){

            if (window.event) {
                return evt.keyCode;
            } else if (evt.which) {
                return evt.which;
            }
        },

        getCharFrom: function (key) {
            // Firefox returns undefined for
            // control characters such as tab,
            // return, backspace, etc.
            if (key === undefined ||
                key < 32) {
                return null;
            } else {
                return String.fromCharCode(key);
            }
        },

        getChar: function (evt) {
            var key = this.getKey(evt);
            var chr = this.getCharFrom(key);
            console.debug(key + " = " + chr);
            return chr;
        },

        invalidChar: function (chr, expected) {
            // checks characater against list of 
            // expected/acceptable characters
            return expected.indexOf(chr) < 0;
        }

    });

    //
    // Controls Textbox Input
    //

    function inputMaskDecimal(){
        var decimalCharacters = "0123456789.",
            mask = new InputMask();

        return function(evt){

            var chr = mask.getChar(evt);
            var current = evt.target.value;

            if (chr === null) {
                return;
            }

            if ( mask.invalidChar(chr, decimalCharacters) || ( chr === "." && current.indexOf(".") >= 0 )) {
                evt.preventDefault();
            }
        };
    }


    function inputMaskPhone(){
        var phoneCharacters = "0123456789.-",
            mask = new InputMask();

        return function(evt){
            var chr = mask.getChar(evt);

            if (chr === null) {
                return;
            }

            if ( mask.invalidChar(chr, phoneCharacters) ) { 
                evt.preventDefault();
            }
        };
    }


    function inputMaskEmail(){
        var emailCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-._+@",
            mask = new InputMask();

        return function(evt){
            var chr = mask.getChar(evt);
            if (chr === null) {
                return true;
            }
            var current = evt.target.value;
            console.debug(current);
            if( mask.invalidChar(chr, emailCharacters) ||  
                (chr == "@" && current.indexOf("@") >=0 ) ) {
               evt.preventDefault();
            }
        };
    }

    return {
        inputMaskDecimal: inputMaskDecimal,
        inputMaskPhone: inputMaskPhone,
        inputMaskEmail: inputMaskEmail
    };

});
