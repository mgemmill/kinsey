var profile = (function(){
	var testResourceRe = /^kinsey\/tests\//,

        testFiles = function(filename, mid) {
            var list = {
                "kinsey/testing": true,
                "kinsey/tests": true
            };
            return (mid in list);
        },

		copyOnly = function(filename, mid){
			var list = {
                "kinsey/package.json": true,
                "kinsey/app.profile.json": true
			};
			return (mid in list); 
		};

	return {

		resourceTags:{
			test: function(filename, mid){
				return testResourceRe.test(mid) || testFiles(filename, mid);
			},

			copyOnly: function(filename, mid){
				return copyOnly(filename, mid);
			},

			amd: function(filename, mid){
				return !testResourceRe.test(mid) && !copyOnly(filename, mid) && /\.js$/.test(filename);
			}
		}

	};
})();
