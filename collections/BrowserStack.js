//>> pure-amd
define([
    'dojo', 
    'dojo/topic',
    'dojo/_base/declare',
    'dojo/_base/lang'
], function (
    dojo,
    topic
) {


    var BrowserStack = dojo.declare('kinsey/collections/BrowserStack', [], {
        // A Doubly-Linked List

        FORWARD_ENABLED: 'browserstack-forward-enabled',

        FORWARD_DISABLED: 'browserstack-forward-disabled',

        BACK_ENABLED: 'browsestack-back-enabled',

        BACK_DISABLED: 'browsestack-back-disabled',

        constructor: function(array) {
            this.length = 0;
            this.maxLength = 100;
            this.current = null;
            if (array) {
                dojo.forEach(array, dojo.hitch(this, function (obj) {
                    this.push(obj); 
                }));
            }
        },

        publishState: function () {
            if ( this.countToStart() > 0 ) {
                topic.publish(this.BACK_ENABLED);
            } else {
                topic.publish(this.BACK_DISABLED);
            }
            if ( this.countToEnd() > 0 ) {
                topic.publish(this.FORWARD_ENABLED);
            } else {
                topic.publish(this.FORWARD_DISABLED);
            }
        },

        push: function(object) {
            // append object to the end of the list
            var node = new BrowserStack.Node(object);
            if ( this.current === null ) {
                // create the first node.
                this.current = node;
            } else {
                this.chop();
                this.current.next = node;
                node.previous = this.current;
                this.current = node;
            }
            this.length += 1;
            this.publishState();
        },

        chop: function () {
            // remove all forward nodes from the current 
            var discard = this.current.next,
                next;
            while ( discard !== null ) {
                next = discard.next;
                discard.previous = null; 
                discard.next = null;
                discard = next;
                this.length -= 1;
            }
        },

        backward: function () {
            if ( !this.current.isFirst() ) {
                this.current = this.current.previous;            
            }
            this.publishState();
            return this.current.data;
        },

       forward: function () {
            if ( !this.current.isLast() ) {
                this.current = this.current.next;   
            }
            this.publishState();
            return this.current.data;
        },

       countToStart: function () {
           var position = this.current,
               count = 0;
           if ( position === null ) {
               return count;
           }
            while ( !position.isFirst() ) {
               count += 1; 
               position = position.previous;
            }
            return count;
       },

       countToEnd: function () {
            var position = this.current,
                count = 0;
           if ( position === null ) {
               return count;
           }
            while ( !position.isLast() ) {
                count += 1;
                position = position.next;
            }
            return count;
       },

       toStart: function () {
            while ( !this.current.isFirst() ) {
                this.backward();
            }
       },

       toEnd: function () {
            while ( !this.current.isLast() ) {
                this.forward();
            }
       },

       toArray: function () {
           var array = [];
            this.toStart(); 
            while ( this.current !== null){
                array.push(this.current.data); 
                if ( this.current.isLast() ) {
                    break;
                }
                this.forward();
            }

            return array;
       }

    });

    BrowserStack.Node = dojo.declare([], {
            constructor: function(data) {
                this.data = data;
                this.previous = null;
                this.next = null;
            },
            isFirst: function() {
                return this.previous === null;
            },
            isLast: function() {
                return this.next === null;
            }
        });

    return BrowserStack;

});
