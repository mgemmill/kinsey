//>> pure-amd
define([
    'dojo', 
    'dojo/_base/lang'
], function (
    dojo
) {


    var CircularList = dojo.declare('kinsey/collections/CircularList', [], {
        // A Doubly-Linked Circular List

        constructor: function() {
            this.current = null;
            this.length = 0;
            this.first = null;
            this.last = null;
        },

        append: function(object) {
            // append object to the end of the list
            var node = new CircularList.Node(object);
            if ( this.current === null ) {
                this.current = node;
                this.current.next = node;
                this.current.previous = node;
            } else {
                node.next = this.current.next;
                this.current.next = node;
                node.previous = this.current;
                this.current = node;
            }
            this.length += 1;
        },
        
        pop: function(object) {
            // remove the current node from the list
            var node = this.current,
                prev = this.current.previous,
                next = this.current.next;
            
            prev.next = next;
            next.previous = prev;
            this.length -= 1;

            return node;
        },

        find: function(object) {
            //var found = this.iter(function(item) {
                //if ( object === item.data ) {
                    //return item;
                //}
            //});
            //return found;
        },

        next: function() {
            var next = this.current.next;
            this.current = next;
            return next;
        },

        iter: function(func) {
            // apply function to list starting at the current node.
            var first = this.current.next,
                current,
                result;

            while ( first !== this.current ) {
                result = func(current);
                if ( result ) {
                    first = this.current;
                } else {
                    first = first.next;
                }
            }

            return result;
        }

    });

    CircularList.Node = dojo.declare([], {
            constructor: function(data) {
                this.data = data;
                this.previous = null;
                this.next = null;
            }
        });

    return CircularList;

});
