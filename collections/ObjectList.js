//>> pure-amd
define([
    'dojo', 
    'dojo/_base/lang'
], function (
    dojo
) {

    var indexOf = dojo.indexOf;

    return dojo.declare('kinsey/collections/ObjectList', [], {
        
        constructor: function(options) {
            options = dojo.declare.safeMixin(this.defaultOptions(), options);
            if (options.mapper) {
                this._mapper = dojo.hitch(this, options.mapper);
            }
            this.count = 0;
            this.currentObject = null;
            this.previousObject = null;
            this.currentIndex = 0;
            this.objectsById = {};
            this.objectsByIndex = [];
            this.objectId = options.objectId;
        },

        defaultOptions: function() {
            return {
                mapper: undefined,
                objectId: "id" 
            };
        },

        getById: function(id) {
            return this.objectsById[id];
        },

        getByIndex: function(index) {
            return this.objectsByIndex[index];
        },

        indexOf: function(object) {
            return indexOf(this.objectsByIndex, object);
        },

        hasObject: function(object) {
            return this.indexOf(object) >= 0; 
        },

        hasId: function(objectId) {
            return objectId in this.objectsById;        
        },

        getRandom: function() {
            var index = Math.floor(this.count * Math.random());
            return this.objectsByIndex[index];
        },

        getFirst: function() {
            return this.objectsByIndex[0];
        },

        getLast: function() {
            return this.objectsByIndex.slice(-1)[0];
        },

        atFirst: function() {
            return this.currentObject === this.getFirst();
        },

        atLast: function() {
            return this.currentObject === this.getLast();
        },

        filter: function(filterFunction) {
            return dojo.filter(this.objectsByIndex, filterFunction);
        },

        _getNextPrev: function(nextId) {
          // console.debug("nextId(" + nextId + ")");
            if (nextId < 0) {
                return this.getLast();
            } else if (nextId > this.count - 1) {
                return this.getFirst();
            } else {
                return this.getByIndex(nextId);
            }
        },

        setCurrent: function(object) {
          // console.debug("set current item: " + object.id);
            this.currentIndex = indexOf(this.objectsByIndex, object);
            this.previousObject = this.currentObject || object;
            this.currentObject = object;
          // console.debug("set current index to: " + this.currentIndex);
            return object;
        },

        peekNext: function () {
            return this._getNextPrev(this.currentIndex + 1);
        },

        peekPrev: function() {
            return this._getNextPrev(this.currentIndex - 1);
        },

        nextCurrent: function () {
            var obj = this.peekNext();
            this.setCurrent(obj);
            return obj;
        },

        prevCurrent: function () {
            var obj = this.peekPrev();
            this.setCurrent(obj);
            return obj;
        },

        length: function() {
            return this.objectsByIndex.length;
        },

        _mapper: function(item) {
            return item;
        },

        add: function (object, mapper) {
            // add a single object to the collection
            mapper = mapper || this._mapper; 
            object = mapper(object);
            this.count += 1;
            this.objectsById[object[this.objectId]] = object;
            this.objectsByIndex.push(object);
        },

        load: function(elements, mapper) {
            var that = this;
            mapper = mapper || this._mapper;
            dojo.forEach(elements, function(item) {
                that.add(item, mapper);
            });
        }

    });

});
