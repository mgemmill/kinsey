//>> pure-amd
define([
    "dojo", 
    "dojox/lang/functional/object",
    "dojo/_base/lang"
], function (
    dojo,
    dojox
    ) {

    var alpha = "abcdefghijklmnopqrstuvwxyz",
        indexOf = dojo.indexOf;

    return dojo.declare('kinsey/collections/ListLikeEnumerator', [], {

        initialLength: 100,

        constructor: function(initialList) {
            // where initialList is actually an object
            this.indexes = indexes = [];
            //for ( var i=0; i < this.initialLength; i++ ) {
                //this.indexes.push(i.toString());
            //}
            dojo.forEach(alpha, function(letter) {
                indexes.push(letter);
            });

            if ( initialList ) {
                this.initialize(initialList);
            }
        },

        initialize: function(initialList) {
            // remove an pre-existing key values for our 
            // initialization list so we can be sure they 
            // do not get used again.
            var keys = dojox.keys(initialList),
                that = this;
            dojo.forEach(keys, function(key) {
                var index = indexOf(that.indexes, key);
                console.debug("removing key: "+key);
                console.debug("removing index: "+index);
                that.indexes.splice(index, 1);
            });
        },

        next: function() {
            return this.indexes.shift();
        }

    });

});
