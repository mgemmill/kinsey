//>> pure-amd
define([
    'dojo', 
    'dojo/_base/lang'
], function (
    dojo
) {


    var LinkedList = dojo.declare('kinsey/collections/LinkedList', [], {
        // A Doubly-Linked List

        constructor: function() {
            this.head = null;
            this.length = 0;
            this.first = null;
            this.last = null;
        },

        append: function(object) {
            // append object to the end of the list
            var node = new LinkedList.Node(object);
            if ( this.first === null ) {
                this.first = node;
                this.last = node;
            } else {
                this.last.next = node;
                node.previous = this.last;
                this.last = node;
            }
            this.length += 1;
        },
        
        insert: function(thisData, newData) {
            var found, next, node;
            found = this.iter(function(item) {
                if ( item.data === thisData ) {
                    return item;
                }
            });
            if ( !found ) {
                throw Error("Data could not be found in the LinkedList!");
            }
            node = new LinkedList.Node(newData);
            next = found.next;
            found.next = node;
            node.previous = found;
            node.next = next;
            next.previous = node;

            return node;
        },

        remove: function(object) {
            var node, prev, next;
            node = this.iter(function(item) {
                if ( item.data == object ) {
                    return item;
                }
            });
            prev = node.previous;
            next = node.next;
            prev.next = next;
            next.previous = prev;

            return node;
        },

        getIndex: function(index) {
            var currentIndex = 0, 
                found;
            if ( index < 0 || index > this.length - 1 ) {
                throw Error("Index out of range.");
            }
            found = this.iter(function(item) {
                if ( currentIndex === index ) {
                    return item;
                }
                currentIndex += 1;
            });
            return found;
        },

        indexOf: function (object) {
            var index = 0; 
            var found = this.iter(function(item) {
                if ( object === item.data ) {
                    return item;
                }
                index += 1;
            });
            return index;
        },

        find: function(object) {
            var found = this.iter(function(item) {
                if ( object === item.data ) {
                    return item;
                }
            });
            return found;
        },

        iter: function(func) {
            // apply function to list starting at the start.
            // if the function returns a result it will
            // shortciruit the iteration process.
            var current = this.first,
                result;

            while ( current !== null ) {
                result = func(current);
                if ( result ) {
                    current = null;
                } else {
                    current = current.next;
                }
            }

            return result;
        }


    });

    LinkedList.Node = dojo.declare([], {
            constructor: function(data) {
                this.data = data;
                this.previous = null;
                this.next = null;
            },
            isFirst: function() {
                return this.previous === null;
            },
            isLast: function() {
                return this.next === null;
            }
        });

    return LinkedList;

});
