//>> pure-amd
define([
    'dojo',
    'dojo/has',
    'dojo/_base/sniff',
    'dojo/string',
    'dojo/dom-construct'
], function (
    dojo,
    has,
    sniff,
    string
    ) {
    
    var subs = string.substitute,
        browsers = {
        'ie':       'IE',
        'quirks':   'Quirks Mode',
        'ff':       'Firefox',
        'mozilla':  'Mozilla',
        'opera':    'Opera',
        'safari':   'Safari',
        'webkit':   'Webkit',
        'chrome':   'Chrome'
    };
    
    function browserVersion ( browserCode ) {
        var version = has(browserCode);
        if ( version ) {
              return subs('${0} ${1}', [browsers[browserCode],version]);
        }
    }

    function displayBrowserVersion() {
        var browserInfo = [];
        function displayBrowser( browserCode ) {
            var version = browserVersion(browserCode);
            if ( version ) browserInfo.push(subs('<pre>${0}</pre>', [version]));
        }
        displayBrowser('ie');
        displayBrowser('ff');
        displayBrowser('opera');
        displayBrowser('safari');
        displayBrowser('chrome');
        displayBrowser('webkit');
        displayBrowser('mozilla');
        displayBrowser('quirks');

        dojo.create('div', { innerHTML: browserInfo.join('<br/>'),
        style: "position:fixed;top:10px;left:10px;color:white;padding:5px;font-size:12px;border:1px solid white;" }, dojo.body(), 'first');
    }

    return {
        version: browserVersion,
        displayVersion: displayBrowserVersion
    };

});
