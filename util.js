//>> pure-amd
define([
    'dojo', 
    './main',
    'dojo/string',
    'dojo/Deferred',
    'dojo/on',
    "dojo/Evented",
    "dojo/io-query",
    'dojo/_base/lang'
], function (
    dojo,
    kinsey,
    string,
    Deferred,
    on,
    Evented
    ) {


    function isEmpty (obj) {
        // to speed things up we could declare this
        // var hasOwnProperty = Object.prototype.hasOwnProperty;
        // and user hasOwnProperty.call(obj, key);
   
        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length && obj.length > 0)    return false;
        if (obj.length && obj.length === 0)  return true;

        for (var key in obj) {
            if (obj.hasOwnProperty(key))    return false;
        }
        return true;
    }

    var Timer = dojo.declare('Timer', [Evented], {
        /*
         *  A better timer class.
         *
         */
            timeout: 1000,

            constructor: function(interval) {
                this.timeout = interval;
                this.event = {
                    ticks: 0
                };
            },
            reset: function() {
                if ( this._handle ) {
                    clearInterval(this._handle);
                    delete this._handle;
                }
            },
            start: function(){
                this.reset();
                this.emit("start", this.event);
                var self = this;
                this._handle = setInterval( function() {
                    self.emit("tick", self.event);
                    self.event.ticks += 1;
                    }, this.timeout );
            },
            stop: function(noEmit){
                this.reset();
                if ( noEmit === undefined ) {
                    this.emit("stop", this.event);
                }
                this.event.ticks = 0;
            }
    }),


    EmailAddress = dojo.declare(null, {
          /* This class is meant as a kind of email obfuscator to keep
           * raw email text from appearing in a pages html. So rather 
           * than hardcoding <a href="mailto:name@emailaddress.com">email</a>
           * in the page, you do this:
           *
           *    email = EmailAddress('name', 'mailaddress.com');
           *    email.insertLink('emailLinkId', 'email');
           *
           * This will insert the above link into a tag with the id `emailLinkId`.
           */
          constructor: function(name, domain) {
              this.name = name;
              this.domain = domain;
          },

          toString: function() {
              return this.name + "@" + this.domain;
          },

          emailTo: function() {
                return 'mailto:' + this.toString();
          },

          insertLink: function(inId, linkName) {
              dojo.create('a', { 
                        innerHTML: linkName,
                        "class": "header-link",
                        href: this.emailTo() },
                        dojo.byId(inId));
          }

    }),

    UrlBuilder = dojo.declare('kinsey/util/UrlBuilder', null, {

        attributes: ['protocol', 'port', 'host', 'path', 'hash', 'query'],

        attrParsers: {
            defaultParser: function (value) {
                return value;
            },
            port: function (port) {
                return Number(port);
            },
            hash: function (hashString) {
                return hashString;
            },
            query: function (query) {
                return this._parseQuery(query);
            }
        },

        constructor: function (options) {
            //console.debug(options);
            this.origOpt = dojo.safeMixin({
                source: "",
                protocol: "http",
                port: 80,
                host: "127.0.0.1",
                path: "",
                hash: "",
                query: {}
            }, options || {});
            //console.debug(this.origOpt);

            //this._parseOptions(this.origOpt);
            this._resetOptions();
            //console.debug("finished parsing constructor options");
        },

        setUrl: function (attr, value) {
            //console.debug('setUrl');
            this.origOpt[attr] = value;
            //console.debug(this.path);
            this._resetOptions();
            //console.debug(this.path);
        },

        _resetOptions: function () {
            this._parseOptions(this.origOpt);
        },

        _parseOptions: function (options) {
            var self = this;

            if ( options === undefined ) return;
            if ( isEmpty(options) ) return;
            //console.debug('---- _parseOptions ---------------');
            //console.debug(options);
            dojo.forEach(this.attributes, function(attr) {
                //console.debug("considering: " + attr);
                option = options[attr];
                if (option !== undefined) {
                    //console.debug("parsing option: " + attr);
                    var parser = self.attrParsers[attr] || self.attrParsers.defaultParser;
                    self[attr] = parser.call(self, option);
                    //console.debug(self[attr]);
                    //console.debug("option set to: " + self[attr]);
                } 
            });
        },

        _parseQuery: function (query) {
            if ( dojo.isObject(query) ) {
                return query;
            } else if ( dojo.isString(query) ) {
                return dojo.queryToObject(query);
            } else {
                return {};
            }
        },

        _protocolString: function () {
            return this.protocol + "://";
        },

        _hostString: function () {
            return this.host; 
        },

        _portString: function () {
            return this.port === 80 ? "" : ":" + this.port;
        },

        _pathString: function (relative) {
            return isEmpty(this.path) ? (relative ? "/" : "") : (this.path[0] === "/" ? this.path : "/" + this.path);
        },

        _queryString: function () {
            return isEmpty(this.query) ? "" : "?" + dojo.objectToQuery(this.query);    
        },

        _hashString: function () {
            return this.hash.length > 0 ? "#" + this.hash : ""; 
        },

        build: function (options) {
            console.debug("BUILD!!!!!");
            try {
                //console.debug(this.path);
                this._parseOptions(options);
                //console.debug(this.path);
                return this._protocolString() + this._hostString() + this._portString() + this._pathString() + this._queryString() + this._hashString();
            } finally {
                this._resetOptions();
            }
        },

        buildRelative: function (options) {
            try {
                this._parseOptions(options);
                return this._pathString("relative") + this._queryString() + this._hashString();
            } finally {
                this._resetOptions();
            }
        }

    }),

    UrlParser = dojo.declare('kinsey/util/UrlParser', null, {

        constructor: function(urlString) {

            this.source = urlString || dojo.doc.location.href;
            var url =  this._parse(this.source);

            this.protocol = url.protocol.replace(':','');
            this.host = url.hostname;
            this.port = url.port;
            this.path = url.pathname.replace(/^([^\/])/,'/$1');
            this.query = url.search;
            this.hash = url.hash.replace('#','');
            this._parsePath(this.path);
            this.params = this._params(this.query); 
        },

        _parsePath: function (pathString) {
            var ender = (pathString.length > 1 && pathString.slice(-1) === "/") ? "/" : "",
                path = pathString.replace(/^\//, ''); 
            path = path.replace(/\/$/, '');
            this.segments = path.split("/");
            this.path = "/" + this.segments.join("/") + ender;
            this.file = this.segments.slice(-1);
        },

        _parse: function (urlString) {
            var regex = /(https?):\/\/([^\/:]+):?(\d+)?(\/[^\?#]*)?(\?[^\?#]*)?(#.*)?/,
                match = regex.exec(urlString);
            return {
                protocol: match[1] || 'http',
                hostname: match[2],
                port: match[3] || '',
                pathname: match[4] || '/',
                search: match[5] || '',
                hash: match[6] || ''
            };
              
        },
        _params: function (queryString) {
            var params = {},
                segments = queryString.replace(/^\?/,'').split('&');
            dojo.forEach(segments, function (segment) {
                if (!segment) return;
                var seg = segment.split('=');
                params[seg[0]] = seg[1];
            });
            return params;
        }
    });
    
    function generateUUID() {
        /*
         * This function was lifted from Briguy37's answer on this StackOverflow question:
         * http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript
         *
         * Also located at this fiddle: http://jsfiddle.net/2MVFd/60/
         * 
         */
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
                d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x7|0x8)).toString(16);
        });
        return uuid;
    }

    function isDate (dateObject) {
        /*
         * Test that the given value is a valide date.
         *
         * A date string will return as a valid date as well as a Date object.
         *
         * The one criteria is that the year be greater than 1899.
         *
         * Javascript Date is pretty liberal in what values it will legitimately
         * accept, so this only really is useful for objects that are not legitimate
         * date strings, any kind of number or a Date object.
         *
         */
        var testValue = new Date(dateObject),
            year,
            month,
            day;
        if ( isNaN(testValue) ) {
            return false;
        }
        try {
            year = testValue.getFullYear();
            month = testValue.getMonth();
            day = testValue.getDate();
            return ( (!isNaN(year) && year > 1899) &&
                     (!isNaN(month) && month >= 0 && month <= 11) &&
                     (!isNaN(day) && day >= 1 && day <= 31)
                   );
        } catch (ex) {
            return false;
        }

    }

    kinsey.isDate = isDate;
    kinsey.isEmpty = isEmpty;
    kinsey.generateUUID = generateUUID;
    kinsey.Timer = Timer;
    kinsey.UrlBuilder = UrlBuilder;
    kinsey.UrlParser = UrlParser;

    return {
        isEmpty: isEmpty,
        isDate: isDate,
        generateUUID: generateUUID,
        Timer: Timer,
        EmailAddress: EmailAddress,
        UrlBuilder: UrlBuilder,
        UrlParser: UrlParser
        };
});
