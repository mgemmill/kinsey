//>> pure-amd
define([
    'dojo', 
    "dojo/Evented",
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/string'
], function (
    dojo,
    Evented
    ) {


    var trim = dojo.string.trim,

        Warning = dojo.declare('kinsey/validation/Warning', [Error], {
            constructor: function(msg) {
                this.name = "Warning";
                this.message = msg;
            }
        }),


        ErrorObject = dojo.declare('kinsey/validation/ErrorObject', [], {

            constructor: function(node) {
                this.node = node;
                this.errors = [];
                this.warnings = [];
                this.count = 0;
            },

            add: function(error) {
                console.debug(error);
                if ( error.name === "Warning" ) {
                    this.warnings.push(error); 
                    this.count += 0.001;
                } else {
                    this.errors.push(error);
                    this.count += 1.0;
                }
            },

            warningsOnly: function() {
                // warinings are OK.
                return this.errors.length === 0 && this.warnings.length >= 0;
            },

            hasErrors: function() {
                // any kind of warning or error
                return this.errors.length > 0 || this.warnings.length > 0;
            },

            fullyValid: function() {
                // no warnings or errors at all 
                return this.errors.length === 0 && this.warnings.length === 0;
            },

            toList: function ( ) { 
                var errorList = [];
                dojo.forEach( this.errors, function ( error ) { 
                    errorList.push( error );
                } );
                dojo.forEach( this.warnings, function ( warning ) { 
                    errorList.push( warning );
                } );
                return errorList;
            } 

        }),


        ErrorCollection = dojo.declare('kinsey/validation/ErrorCollection', [], {

            constructor: function() {
                this.errorObjects = [];
            },

            add: function(error) {
                this.errorObjects.push(error);
            },

            iter: function(handler) {
                dojo.forEach(this.errorObjects, handler);
            },

            toList: function () { 
               var errors = []; 
               dojo.forEach( this.errorObjects, function ( errObj ) { 
                   errors = errors.concat( errObj.toList() );
               });
               return errors;
            },

            warningsOnly: function() {
                var result = true;
                this.iter(function(error) {
                    if ( !error.warningsOnly() ) {
                        result = false;
                    }
                });
                return result;
            },

            hasErrors: function() {
                var result = false;
                this.iter(function(error) {
                    if ( error.hasErrors() ) {
                        result = true;
                    }
                });
                return result;
            }
        }),


        Validator = dojo.declare('kinsey/validation/Validator', [Evented], {

            constructor: function( node, rules ) {

                var validationRules;
                if ( dojo.isArray(rules) ) {
                    validationRules = rules;
                } else if ( dojo.isFunction(rules) ) {
                    validationRules = [rules];
                } else {
                    validationRules = [];
                }

                this.node = node;
                this.rules = validationRules;

            },

            validate: function( object, handler ) {
                var errors = new ErrorObject(this.node),
                    that = this;

                dojo.forEach(this.rules, function(validator) {
                    try {
                        validator(object, that.node);
                    } catch (e) {
                        // attach the associated node to the error
                        e.node = that.node;
                        errors.add(e);
                    }
                });

                if ( errors.hasErrors() ) {
                    handler(this.node, errors);
                } else if ( handler.clear ) {
                    handler.clear(this.node);
                }
                return errors;
            }
        }),
        

        Validate = dojo.declare('kinsey/validation/Validate', [], {
            constructor: function() {
            }
        });

    Validate.email = (function () { 
        var emailRegex = new RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$");
        return function ( value, errMsg ) {
            console.debug("validating for proper email address");
            errMsg = errMsg || "Invalid email address.";
            if ( !emailRegex.test(trim(value)) ) {
                throw new Error(errMsg);
            }
        };
    })();

    Validate.notBlank = function (value, errMsg) {
        errMsg = errMsg || "Required";
        console.debug("validating value is not blank");
        if ( value === "" || trim(value) === "" ) {
            throw new Error(errMsg);
        }
    };

    Validate.equal = function ( valueA, valueB, errMsg ) {
        errMsg = errMsg || "Values must match.";
        console.debug("validating that two values match");
        if ( valueA !== valueB ) {
            throw new Error(errMsg);
        }
    };

    Validate.isDate = function ( value, errMsg ) {
        errMsg = errMsg || "Not a valid date.";
        console.debug("validating for proper date");
        var d = new Date(value);
        if ( d.toString() === "Invalid Date" ) {
            throw new Error(errMsg);
        }
    };

    return {
        ErrorObject: ErrorObject,
        ErrorCollection: ErrorCollection,
        Warning: Warning,
        Validate: Validate,
        Validator: Validator
        };
});
